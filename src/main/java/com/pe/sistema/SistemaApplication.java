package com.pe.sistema;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class SistemaApplication implements CommandLineRunner {

    
    @Autowired
    private JdbcTemplate template;
    
    public static void main(String[] args) {
        SpringApplication.run(SistemaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        template.execute("DROP TABLE MONEDA IF EXISTS");
        template.execute("CREATE TABLE MONEDA(MONEDA VARCHAR(20),MONTO NUMBER(10,2))");
        template.execute("DROP TABLE TIPOCAMBIO IF EXISTS");
        
        template.execute("CREATE TABLE TIPOCAMBIO(ID NUMBER(10) PRIMARY KEY auto_increment,MONTO NUMBER(10,2),MONTOTIPOCAMBIO NUMBER(10,2),MONEDAORIGEN VARCHAR(20),MONEDADESTINO VARCHAR(20),TIPOCAMBIO NUMBER(10,2))");
        
        template.update("INSERT INTO MONEDA(MONEDA,MONTO) VALUES ('DOLAR',3.87) ");
        template.update("INSERT INTO MONEDA(MONEDA,MONTO) VALUES ('SOL',1) ");
        template.update("INSERT INTO MONEDA(MONEDA,MONTO) VALUES ('EURO',4.09) ");
    }

}
