package com.pe.sistema.controller;



import com.pe.sistema.model.BaseResponse;
import com.pe.sistema.model.MonedaDTO;
import com.pe.sistema.model.TipoCambioDTO;
import com.pe.sistema.model.TipoCambioRequest;
import com.pe.sistema.model.TipoCambioResponse;
import com.pe.sistema.service.TipoCambioService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Usuario
 */

@RestController
@RequestMapping("/api/sistema")
public class TipoCambioController {
    final TipoCambioService tipoCambioService;

    public TipoCambioController(TipoCambioService tipoCambioService) {
        this.tipoCambioService = tipoCambioService;
    }
    
    
    @GetMapping("/listarMoneda")
    public List<MonedaDTO> listarMoneda() {
        return this.tipoCambioService.listarMoneda();
    }
    
    @GetMapping("/listarOperaciones")
    public List<TipoCambioResponse> listarOperaciones() {
        return this.tipoCambioService.listarOperaciones();
    }
    
    @PostMapping("/updateMontoMoneda")
    public BaseResponse updateMontoMoneda(@Validated @RequestBody MonedaDTO request) {
        return this.tipoCambioService.updateMontoMoneda(request);
    }
    
    @PostMapping("/agregarOperacion")
    public TipoCambioResponse agregarOperacion(@Validated @RequestBody TipoCambioRequest request) {
        return this.tipoCambioService.agregarOperacion(request);
    }
}
