package com.pe.sistema.exception;

/**
 *
 * @author Usuario
 */
public class ApiBadRequest extends RuntimeException{
    public ApiBadRequest(String message) {
        super(message);
    }
}
