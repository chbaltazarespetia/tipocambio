/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.sistema.model;

import lombok.Data;

/**
 *
 * @author chbal
 */

@Data
public class BaseResponse {
    private String respuesta;
    private String mensaje;
}
