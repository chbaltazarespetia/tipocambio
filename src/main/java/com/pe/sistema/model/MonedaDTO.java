/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pe.sistema.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Usuario
 */

@Data
public class MonedaDTO {
    @NotBlank(message = "Variable debe tener valor")
    private String moneda;
    
    @NotNull
    private double monto;
}
