/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.sistema.model;

/**
 *
 * @author chbal
 */
public class TipoCambioDTO {
    private int id;
    private double monto;
    private double montoTipoCambio;
    private String monedaOrigen;
    private String monedaDestino;
    private double tipoCambio;
}
