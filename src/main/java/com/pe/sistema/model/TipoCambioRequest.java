/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.sistema.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author chbal
 */

@Data
public class TipoCambioRequest {
    
    @NotNull
    private double monto;
    
    @NotBlank(message = "Variable debe tener valor")
    private String monedaOrigen;
    
    @NotBlank(message = "Variable debe tener valor")
    private String monedaDestino;
}
