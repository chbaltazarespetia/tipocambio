package com.pe.sistema.repository;

import com.pe.sistema.model.BaseResponse;
import com.pe.sistema.model.MonedaDTO;
import com.pe.sistema.model.TipoCambioDTO;
import com.pe.sistema.model.TipoCambioRequest;
import com.pe.sistema.model.TipoCambioResponse;
import java.util.List;

/**
 *
 * @author Usuario
 */
public interface TipoCambioRepository {
    List<MonedaDTO> listarMoneda();
    List<TipoCambioResponse> listarOperaciones();
    double ObtenerMonto(String Moneda);
    BaseResponse updateMontoMoneda(MonedaDTO request);
    TipoCambioResponse agregarOperacion(TipoCambioRequest request);
}
