package com.pe.sistema.repository;

import com.pe.sistema.exception.ApiBadRequest;
import com.pe.sistema.model.BaseResponse;
import com.pe.sistema.model.MonedaDTO;
import com.pe.sistema.model.TipoCambioDTO;
import com.pe.sistema.model.TipoCambioRequest;
import com.pe.sistema.model.TipoCambioResponse;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Usuario
 */
@Repository
@AllArgsConstructor
public class TipoCambioRepositoryImpl implements TipoCambioRepository {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public List<MonedaDTO> listarMoneda() {
        String sql = "SELECT MONEDA,MONTO FROM MONEDA";
        try {
            return this.jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(MonedaDTO.class));
        } catch (Exception ex) {
            throw new ApiBadRequest("Algo salio mal: " + ex.getMessage());
        }
    }

    @Override
    public List<TipoCambioResponse> listarOperaciones() {

            String sql = "SELECT monto,montoTipoCambio,monedaOrigen,monedaDestino,tipoCambio FROM TIPOCAMBIO";
        try {
            return this.jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(TipoCambioResponse.class));
        } catch (Exception ex) {
            throw new ApiBadRequest("Algo salio mal: " + ex.getMessage());
        }
    }

    @Override
    public double ObtenerMonto(String Moneda) {
        String sql = "SELECT MONTO FROM MONEDA M WHERE MONEDA = '" + Moneda + "'";
        try {
            return this.jdbcTemplate.queryForObject(sql, double.class);
        } catch (Exception ex) {
            throw new ApiBadRequest("Algo salio mal: " + ex.getMessage());
        }
    }

    @Override
    public BaseResponse updateMontoMoneda(MonedaDTO request) {
        String sql = " UPDATE MONEDA SET MONTO =? WHERE MONEDA =?";
        BaseResponse br = new BaseResponse();
        try {
            Object[] params = { request.getMonto(),
                                request.getMoneda()};
            int[] types = {Types.DOUBLE, Types.VARCHAR};
            int respuesta =this.jdbcTemplate.update(sql,params,types);
            System.out.println(respuesta + " row(s) updated.");
            if (respuesta > 0) {
                br.setRespuesta("1");
                br.setMensaje("Registrado con éxito");
            } else {
                br.setRespuesta("0");
                br.setMensaje("No se encontro ninguna fila para actualizar");
            }
            return br;

        } catch (Exception ex) {
            br.setRespuesta("0");
            br.setMensaje("Error en la transición "+ex.getMessage());
            return br;
        }
    }

    @Override
    public TipoCambioResponse agregarOperacion(TipoCambioRequest request) {
        try {
            double montoMonedaOrigen = ObtenerMonto(request.getMonedaOrigen());
            double montoMonedaDestino = ObtenerMonto(request.getMonedaDestino());

            double totalMontoCambio = request.getMonto() * montoMonedaOrigen / montoMonedaDestino;
            totalMontoCambio = Math.round(totalMontoCambio * 100.0) / 100.0;
            
            double tipoCambio = montoMonedaOrigen/montoMonedaDestino;
            tipoCambio = Math.round(tipoCambio * 100.0) / 100.0;
            
            String sql = "INSERT INTO TIPOCAMBIO(MONTO,MONTOTIPOCAMBIO,MONEDAORIGEN,MONEDADESTINO,TIPOCAMBIO)"
                       + "VALUES(?,?,?,?,?)";
            int respuesta = this.jdbcTemplate.update(sql,
                                                     request.getMonto(),
                                                     totalMontoCambio,
                                                     request.getMonedaOrigen(),
                                                     request.getMonedaDestino(),
                                                     tipoCambio);
            
            TipoCambioResponse tc = new  TipoCambioResponse();
            
            System.out.println(respuesta + " row(s) inserted.");
            if (respuesta > 0) {
                tc.setMonto(request.getMonto());
                tc.setMontoTipoCambio(totalMontoCambio);
                tc.setMonedaOrigen(request.getMonedaOrigen());
                tc.setMonedaDestino(request.getMonedaDestino());
                tc.setTipoCambio(tipoCambio);
            }
            return tc;
        } catch (Exception ex) {
            throw new ApiBadRequest("Algo salio mal: " + ex.getMessage());
        }
    }

}
