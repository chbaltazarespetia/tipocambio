package com.pe.sistema.service;

import com.pe.sistema.model.MonedaDTO;
import com.pe.sistema.model.TipoCambioRequest;
import com.pe.sistema.model.TipoCambioResponse;
import com.pe.sistema.model.BaseResponse;
import com.pe.sistema.model.TipoCambioDTO;
import com.pe.sistema.repository.TipoCambioRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 *
 * @author Usuario
 */

@Service
@AllArgsConstructor
public class TipoCambioService {
    private TipoCambioRepository tipoCambioRepository;
    
    public List<MonedaDTO> listarMoneda() {
        return this.tipoCambioRepository.listarMoneda();
    }
    
    public List<TipoCambioResponse> listarOperaciones() {
        return this.tipoCambioRepository.listarOperaciones();
    }
    
    public BaseResponse updateMontoMoneda(MonedaDTO request) {
        return this.tipoCambioRepository.updateMontoMoneda(request);
    }
    
    public TipoCambioResponse agregarOperacion(TipoCambioRequest request) {
        return this.tipoCambioRepository.agregarOperacion(request);
    }
}
